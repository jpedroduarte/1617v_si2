﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Procedures
{
    class EventSubscription
    {
        public static void Subscribe(int id_evento, int ano, int nif, int? desconto_id)
        {
            using (var context = new SoAventuraEntities())
            {
                context.sp_subscricao_cliente_existente(id_evento,ano,nif,desconto_id);
            }
        }
    }
}
