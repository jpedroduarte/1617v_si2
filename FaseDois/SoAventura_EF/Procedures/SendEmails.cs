﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Procedures
{
    class SendEmails
    {
        public static void EventInDays(int days)
        {
            using (var context = new SoAventuraEntities())
            {
                context.sp_warn_client_event_in_x_days(days);
            }
        }
    }
}
