﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Procedures
{
    class ListCanceledEvents
    {
        public static Dictionary<string, int> FromInterval(DateTime initial, DateTime final)
        {
            Dictionary<string, int> eventCount = new Dictionary<string, int>();
            using (var context = new SoAventuraEntities())
            {
                var events = context.sp_list_canceled_events_by_type(initial, final);
                foreach (sp_list_canceled_events_by_type_Result listed in events)
                {
                    eventCount.Add(listed.desporto,listed.count.Value);
                }
            }
            return eventCount;
        }
    }
}
