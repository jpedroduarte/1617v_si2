﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Procedures
{
    class SubPayments
    {
        public static void Pay(int idSubscricao)
        {
            using (var context = new SoAventuraEntities())
            {
                context.sp_pagamento_subscricao(idSubscricao);
            }
        }
    }
}
