﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Procedures
{
    class AveragePayment
    {
        public static Dictionary<int, decimal> FromInterval(int initial, int final)
        {
            Dictionary<int, decimal> paymentList = new Dictionary<int, decimal>();
            using (var context = new SoAventuraEntities())
            {
                var payments = context.sp_get_payments_by_year(initial, final);

                foreach (var listed in payments) {
                    paymentList.Add(listed.Column1.Value, listed.Average.Value);
                }
            }
            return paymentList;
        }
    }
}
