﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Procedures
{
    public class ListAvailableEvents
    {
        public static ICollection<Evento> FromInterval(DateTime initial, DateTime final)
        {
            ICollection<Evento> eventList = new List<Evento>();
            using (var context = new SoAventuraEntities())
            {
                var events = context.sp_available_event_list_in_x_days(initial,final);
                foreach (sp_available_event_list_in_x_days_Result listed in events)
                {
                    Evento evento = (from e in context.Evento
                                     where e.ano == listed.ano && e.id == listed.id
                                     select e).SingleOrDefault();
                    if (evento != null) {

                        eventList.Add(evento);
                    }
                }
            }
            return eventList;
        }
    }
}
