﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Procedures
{
    class EventStatus
    {
        public static void update()
        {
            using (var context = new SoAventuraEntities())
            {
                context.sp_update_Evento_status();
            }
        }
    }
}
