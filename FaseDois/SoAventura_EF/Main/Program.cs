﻿using SoAventura_EF.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoAventura_EF.Utils;
using SoAventura_EF.Procedures;

namespace SoAventura_EF
{
    class Program
    {
        static void Main(string[] args)
        {
            String value = String.Empty;
            do
            {
                Console.Write("**SoAventura EF**\n\n1. Create new Subscription by known Client\n2. Pay a Subscription\n3. Change Events state\n4. Send mail warnings\n5. List cancelled Events\n6. List Events with available space\n7. Obtain payments by year\n8. Get total payed by client\n9. Delete Events by year\n10. Exit\n\n.: ");
                value = Console.ReadLine();

                switch (value)
                {
                    case "1":
                        {
                            int id_evento = GetData.getInt("ID event");
                            int ano = GetData.getInt("year");
                            int nif = GetData.getInt("nif");
                            int? desconto_id = GetData.getIntNullable("ID discount");
                            EventSubscription.Subscribe(id_evento, ano, nif, desconto_id);
                        }
                        break;
                    case "2":
                        {
                            int id_subscricao = GetData.getInt("ID subscription");
                            SubPayments.Pay(id_subscricao);
                        }
                        break;
                    case "3":
                        {
                            EventStatus.update();
                            Console.WriteLine("Events status updated.");
                        }
                        break;
                    case "4":
                        {
                            int days = GetData.getInt("days");
                            SendEmails.EventInDays(days);
                            Console.WriteLine("Emails sent.");
                        }
                        break;
                    case "5":
                        {
                            DateTime initial = GetData.getDate("initial date");
                            DateTime final = GetData.getDate("final date");
                            PrintResults.print(ListCanceledEvents.FromInterval(initial, final), "Sports", "Count");
                        }
                        break;
                    case "6":
                        {
                            DateTime initial = GetData.getDate("initial date");
                            DateTime final = GetData.getDate("final date");
                            PrintResults.print(ListAvailableEvents.FromInterval(initial, final));
                        }
                        break;
                    case "7":
                        {
                            int initial = GetData.getInt("initial year");
                            int final = GetData.getInt("final year");
                            PrintResults.print(AveragePayment.FromInterval(initial, final), "Year", "Average");
                        }
                        break;
                    case "8":
                        {
                            int type = GetData.getInt("type");
                            DateTime init = GetData.getDate("initial date");
                            DateTime end = GetData.getDate("final year");
                            PrintResults.print(TotalPaidByClientsBySportType.exec(type, init, end), "NIF", "Total Paid");
                        }
                        break;
                    case "9":
                        {
                            int year = GetData.getInt("year");
                            Console.WriteLine("Events deleted: " + DeleteAllEventsByYear.exec(year));
                        }
                        break;
                    case "10":
                        {

                        }
                        break;
                    default:
                        Console.WriteLine("Please write a valid number");
                        break;
                }
                Console.WriteLine();
            }
            while (value != "10");
        }
    }
}
