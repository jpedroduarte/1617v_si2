﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Main
{
    class TotalPaidByClientsBySportType
    {
        public static Dictionary<int, decimal> exec(int tipo, DateTime init, DateTime end)
        {
            Dictionary<int, decimal> dic = new Dictionary<int, decimal>();

            using (var ctx = new SoAventuraEntities())
            {
                var sub = (from e in ctx.Evento
                          where e.data_limite_pagamento >= init
                          select e.Subscricao);

                foreach (var b in sub)
                {
                    foreach (Subscricao c in b)
                    {
                        //valor_a_pagar a não ser testado null pois é garantido por regra de negócio.
                        if (c.pago && c.data_pagamento >= init && c.data_pagamento <= end)
                        {
                            if (dic.ContainsKey(c.NIF)) dic[c.NIF] += c.valor_a_pagar.Value;
                            else dic.Add(c.NIF, c.valor_a_pagar.Value);
                        }
                    }
                }
            }

            return dic;
        }
    }
}
