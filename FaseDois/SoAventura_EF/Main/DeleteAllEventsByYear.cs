﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura_EF.Main
{
	public class DeleteAllEventsByYear
	{
		public static int exec(int ano)
		{
			int count = 0;
			using (var ctx = new SoAventuraEntities())
			{
				ctx.Configuration.AutoDetectChangesEnabled = false;
				(from e in ctx.Evento where e.ano == ano select e)
					.ToList<Evento>()
					.ForEach(evento =>{
						++count;
						evento.Subscricao.ToList<Subscricao>().ForEach(sub =>{
							Factura f = sub.Factura.FirstOrDefault<Factura>();
							if (f != null)
								ctx.Factura.Remove(f);
							ctx.Subscricao.Remove(sub);
						});
						ctx.Evento.Remove(evento);
					});
				ctx.SaveChanges();
			}
			return count;
		}
	}
}
