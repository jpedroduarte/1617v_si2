﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoAventura.Main;

using System.Transactions;

namespace ADO.NET_Tests.Features
{
	[TestClass]
	public class DeleteAllEventsByYear_Test
	{
		[TestMethod]
		public void DeleteAllEventsByYear_Test_0()
		{
			using (var ts = new TransactionScope(TransactionScopeOption.Required,
					new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
			{

				int changes = DeleteAllEventsByYear.exec(2016);
				Assert.AreEqual(3, changes);
				//Not invoking TransactionScope.Complete() so that the changes to the database are not committed.
			}
		}
	}
}
