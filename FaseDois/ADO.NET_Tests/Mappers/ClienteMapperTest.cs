﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoAventura.Entities;
using SoAventura.SpecificDAL;

namespace ADO.NET_Tests
{
	[TestClass]
	public class ClienteMapperTest
	{
		[TestMethod]
		public void clienteMapper_Test_0(){
			MapperCliente m = new MapperCliente();
			Cliente c = m.Read(0);
			Assert.IsNotNull(c);
		}

		[TestMethod]
		public void clienteMapper_Test_1()
		{
			MapperCliente m = new MapperCliente();
			Cliente c = m.Read(-1);
			Assert.IsNull(c);
		}
	}
}
