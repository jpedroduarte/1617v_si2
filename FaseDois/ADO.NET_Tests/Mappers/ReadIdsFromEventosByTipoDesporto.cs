﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoAventura.Entities;
using SoAventura.SpecificDAL;
using System.Collections.Generic;
using System.Linq;

namespace ADO.NET_Tests.Mappers
{
	[TestClass]
	public class ReadIdsFromEventosByTipoDesporto
	{
		[TestMethod]
		public void ReadIdsFromEventosByTipoDesporto_Test_0()
		{
			MapperEvento bla = new MapperEvento();
			ICollection<Evento> nu = bla.ReadAllEventosByTipoDesporto(2);
			Assert.AreEqual(3, nu.Count);
		}

		[TestMethod]
		public void ReadIdsFromEventosByTipoDesporto_Test_1()
		{
			MapperEvento bla = new MapperEvento();
			ICollection<Evento> nu = bla.ReadAllEventosByTipoDesporto(6);
			Assert.AreEqual(0, nu.Count);
		}
	}
}
