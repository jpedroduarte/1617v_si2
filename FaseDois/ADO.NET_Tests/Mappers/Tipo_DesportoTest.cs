﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoAventura.Entities;
using SoAventura.SpecificDAL;

namespace ADO.NET_Tests.Mappers
{
	[TestClass]
	public class Tipo_DesportoTest
	{
		[TestMethod]
		public void Tipo_Desporto_Test_0()
		{
			MapperTipo_Desporto m = new MapperTipo_Desporto();
			Tipo_Desporto t = m.Read(2);
			Assert.IsNotNull(t);
		}

		[TestMethod]
		public void Tipo_Desporto_Test_1()
		{
			MapperTipo_Desporto m = new MapperTipo_Desporto();
			Tipo_Desporto t = m.Read(-1);
			Assert.IsNull(t);
		}
	}
}
