﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoAventura.Entities;
using SoAventura.SpecificDAL;

namespace ADO.NET_Tests.Mappers
{
	[TestClass]
	public class EventoMapperTest
	{
		[TestMethod]
		public void ReadEventosTest_0()
		{
			int id = 0, ano = 0;
			MapperEvento bla = new MapperEvento();
			Evento e = bla.Read(Tuple.Create(id, ano));
			Assert.IsNull(e);
		}

		[TestMethod]
		public void ReadEventos_Test_2()
		{
			int id = 1, ano = 2016;
			MapperEvento bla = new MapperEvento();
			Evento e = bla.Read(Tuple.Create(id, ano));
			Assert.IsNotNull(e);
		}
	}
}
