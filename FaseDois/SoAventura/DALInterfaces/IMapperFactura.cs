﻿using SoAventura.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura.DALInterfaces
{
    public interface IMapperFactura
    {
        Factura ReadByIdSub(int id);
    }
}
