﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoAventura.Entities;
using SoAventura.DALAbstraction;

namespace SoAventura.DALInterfaces
{
	public interface IMapperEvento
	{
		ICollection<Evento> ReadAllEventosByTipoDesporto(int id_tipo_desporto);
		ICollection<Evento> ReadAllEventosByAno(int ano);
	}
}
