﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoAventura.Entities;
using SoAventura.SpecificDAL;

namespace SoAventura.Main
{
	public class DeleteAllEventsByYear
	{
		public static int exec(int year)
		{
			MapperEvento eventMapper = new MapperEvento();
			ICollection<Evento> events = eventMapper.ReadAllEventosByAno(year);
			events.ToList().ForEach((ev) => eventMapper.Delete(ev));
			return events.Count;
		}
	}
}
