﻿using SoAventura.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoAventura.SpecificDAL;

namespace SoAventura.Main
{
    class TotalPaidByClientsBySportType
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Dictionary<int, decimal> exec(int tipo, DateTime init, DateTime end)
        {
            Tipo_Desporto a = new MapperTipo_Desporto().Read(tipo);
            Dictionary<int,decimal> dic = new Dictionary<int,decimal>();

            foreach (Evento b in a.Eventos)
            {
                if (b.id_tipo_desporto == tipo && b.data_limite_pagamento >= init)
                {
                    foreach (Subscricao c in b.Subscricoes)
                    {
                        if (c.pago && c.data_pagamento >= init && c.data_pagamento <= end)
                        {
                            if (dic.ContainsKey(c.NIF)) dic[c.NIF] += c.valor_a_pagar;
                            else dic.Add(c.NIF, c.valor_a_pagar);
                        }
                    }
                }
            }

            return dic;
        }
    }
}
