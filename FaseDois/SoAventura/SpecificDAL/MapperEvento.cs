﻿using SoAventura.DALInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using SoAventura.DALAbstraction;
using SoAventura.Entities;
using System.Transactions;
using System.Data.SqlClient;
using System.Data;
using SoAventura.Utils;

namespace SoAventura.SpecificDAL
{
	public class MapperEvento : AbstractMapper<Evento, Tuple<int, int>>, IMapperEvento
	{
		private EventFactory eventFactory = new EventFactory();
		private static readonly log4net.ILog log =
			log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public override void Create(Evento entity)
		{
			throw new NotImplementedException();
		}

		public override Evento Read(Tuple<int, int> key)
		{
			Evento evento = null;
			try
			{
				using (var ts = new TransactionScope(TransactionScopeOption.Required,
					new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
				{
					SqlCommand cmd = new SqlCommand();
					cmd.CommandText = "select * from Evento where id = @id AND ano = @ano";
					cmd.Parameters.AddWithValue("@id", key.Item1);
					cmd.Parameters.AddWithValue("@ano", key.Item2);
					using (var cn = new SqlConnection(this.cs))
					{
						cmd.Connection = cn;
						cn.Open();
						SqlDataReader reader = cmd.ExecuteReader();
						if (!reader.Read()) return null;
						evento = eventFactory.create(reader);
					}
					evento.Subscricoes = new MapperSubscricao().ReadMultByIdAno(Tuple.Create<int, int>(evento.id, evento.ano));					
					ts.Complete();
				}
			}
			catch (Exception e)
			{
				log.Error(e.Message);
			}
			return evento;
		}

		public override void Update(Evento entity)
		{
			throw new NotImplementedException();
		}

		public override void Delete(Evento entity)
		{
			using (var ts = new TransactionScope(TransactionScopeOption.Required))
			{
				MapperSubscricao subsMapper = new MapperSubscricao();
				try
				{
					//Delete all Subscriptions from the event
					entity.Subscricoes.ToList().ForEach((sub) => subsMapper.Delete(sub));

					SqlCommand cmd = new SqlCommand();
					cmd.CommandText = "delete from Evento where id = @id AND ano = @ano";
					cmd.Parameters.AddWithValue("@id", entity.id);
					cmd.Parameters.AddWithValue("@ano", entity.ano);
					using (var cn = new SqlConnection(this.cs))
					{
						cmd.Connection = cn;
						cn.Open();
						cmd.ExecuteNonQuery();
					}
					ts.Complete();
				}
				catch (Exception e)
				{
					log.Error(e.Message);
				}
			}
		}

		public ICollection<Evento> ReadAllEventosByTipoDesporto(int id_tipo_desporto)
		{
			ICollection<Evento> list = new List<Evento>();
			try
			{
				using (var ts = new TransactionScope(TransactionScopeOption.Required,
					new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
				{
					SqlCommand cmd = new SqlCommand();
					cmd.CommandText = "select * from Evento where id_tipo_desporto = @id";
					cmd.Parameters.AddWithValue("@id", id_tipo_desporto);
					using (var cn = new SqlConnection(this.cs))
					{
						cmd.Connection = cn;
						cn.Open();
						SqlDataReader reader = cmd.ExecuteReader();
						while (reader.Read())
						{
							list.Add(eventFactory.create(reader));
						}
					}

					((List<Evento>)list).ForEach( (evento) => 
						evento.Subscricoes = new MapperSubscricao().ReadMultByIdAno(Tuple.Create<int, int>(evento.id, evento.ano)));					
					ts.Complete();
				}
			}
			catch (Exception e)
			{
				log.Error(e.Message);
			}
			return list;
		}

		public ICollection<Evento> ReadAllEventosByAno(int ano)
		{
			ICollection<Evento> list = new List<Evento>();
			try
			{
				using (var ts = new TransactionScope(TransactionScopeOption.Required,
					new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
				{
					SqlCommand cmd = new SqlCommand();
					cmd.CommandText = "select * from Evento where ano = @id";
					cmd.Parameters.AddWithValue("@id", ano);
					using (var cn = new SqlConnection(this.cs))
					{
						cmd.Connection = cn;
						cn.Open();
						SqlDataReader reader = cmd.ExecuteReader();
						while (reader.Read())
						{
							list.Add(eventFactory.create(reader));
						}
					}

					((List<Evento>)list).ForEach((evento) =>
						evento.Subscricoes = new MapperSubscricao().ReadMultByIdAno(Tuple.Create<int, int>(evento.id, evento.ano)));
					ts.Complete();
				}
			}
			catch (Exception e)
			{
				log.Error(e.Message);
			}
			return list;
		}
	}
}
