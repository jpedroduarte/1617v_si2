﻿using SoAventura.DALAbstraction;
using SoAventura.DALInterfaces;
using SoAventura.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using SoAventura.SpecificDAL;
using SoAventura.Utils;

namespace SoAventura.SpecificDAL
{
	public class MapperTipo_Desporto : AbstractMapper<Tipo_Desporto, int>
	{
		private Tipo_DesportoFactory tipoFactory = new Tipo_DesportoFactory();
		private static readonly log4net.ILog log =
			log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		
		public override void Create(Tipo_Desporto a)
        {
			throw new NotImplementedException();
        }

		public override void Delete(Tipo_Desporto entity)
        {
            throw new NotImplementedException();
        }

		public override Tipo_Desporto Read(int id)
        {
			Tipo_Desporto tipo = null;
			try
			{
				using (var ts = new TransactionScope(TransactionScopeOption.Required,
					new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
				{
					SqlCommand cmd = new SqlCommand();
					cmd.CommandText = "select * from Tipo_Desporto where id = @id";
					cmd.Parameters.AddWithValue("@id", id);
					using (var cn = new SqlConnection(this.cs))
					{
						cmd.Connection = cn;
						cn.Open();
						SqlDataReader reader = cmd.ExecuteReader();
                        if (!reader.Read()) return null;
						tipo = tipoFactory.create(reader);
					}
					tipo.Eventos = new MapperEvento().ReadAllEventosByTipoDesporto(id);
					ts.Complete();
				}
			}
			catch (Exception e)
			{
				log.Error(e.StackTrace);
			}
			return tipo;
        }

		public override void Update(Tipo_Desporto entity)
        {
            throw new NotImplementedException();
        }

	}
}
