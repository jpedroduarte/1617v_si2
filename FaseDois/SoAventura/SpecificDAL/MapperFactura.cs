﻿using SoAventura.DALAbstraction;
using SoAventura.DALInterfaces;
using SoAventura.Entities;
using SoAventura.Utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.SpecificDAL
{
    public class MapperFactura : AbstractMapper<Factura, Tuple<int, int>>, IMapperFactura
    {

        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void Create(Factura entity)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Factura a)
        {
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "DELETE FROM Factura WHERE id = @id AND ano = @ano";
                    cmd.Parameters.AddWithValue("@id", a.id);
                    cmd.Parameters.AddWithValue("@ano", a.ano);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        cmd.ExecuteNonQuery();

                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
        }

        public override Factura Read(Tuple<int, int> key)
        {
            Factura a = null;

            try
            {

                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT * FROM Factura WHERE id = @id AND ano = @ano";

                    cmd.Parameters.AddWithValue("@id", key.Item1);
                    cmd.Parameters.AddWithValue("@ano", key.Item2);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        SqlDataReader data = cmd.ExecuteReader();

                        if (data.Read()) a = new FacturaFactory().create(data);
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            return a;
        }

        public Factura ReadByIdSub(int id)
        {
            Factura a = null;

            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT * FROM Factura WHERE id_subscricao = @id_subscricao";

                    cmd.Parameters.AddWithValue("@id_subscricao", id);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        SqlDataReader data = cmd.ExecuteReader();

                        if (data.Read()) a = new FacturaFactory().create(data);
                    }
                    ts.Complete();
                }   
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            return a;
        }

        public override void Update(Factura entity)
        {
            throw new NotImplementedException();
        }
    }
}
