﻿using SoAventura.DALAbstraction;
using SoAventura.DALInterfaces;
using SoAventura.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.SpecificDAL
{
    public class MapperEmail : AbstractMapper<Email, int>, IMapperEmail
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void Create(Email entity)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Email entity)
        {
            throw new NotImplementedException();
        }

        public override Email Read(int id)
        {
            Email email = null;
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT * FROM Email_Log WHERE id = @id";

                    cmd.Parameters.AddWithValue("@id", id);

                    using (var cn = new SqlConnection(cs))
                    {

                        cmd.Connection = cn;
                        cn.Open();

                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            int i = 0;
                            email = new Email();
                            email.id = reader.GetSqlInt32(i++).Value;
                            email.nif = reader.GetSqlInt32(i++).Value;
                            email.mensagem = reader.GetSqlString(i++).Value;
                        }
                        reader.Close();
                        cn.Close();
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            return email;
        }

        public override void Update(Email entity)
        {
            throw new NotImplementedException();
        }

        public ICollection<Email> ReadEmailsByNIF(int nif)
        {
            ICollection<Email> idList = new List<Email>();

            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT * FROM Email_Log WHERE nif = @nif";

                    cmd.Parameters.AddWithValue("@nif", nif);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            int i = 0;
                            Email email = new Email();
                            email.id = reader.GetSqlInt32(i++).Value;
                            email.nif = reader.GetSqlInt32(i++).Value;
                            email.mensagem = reader.GetSqlString(i++).Value;
                            idList.Add(email);
                        }
                        reader.Close();
                        cn.Close();
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            return idList;
        }
    }
}
