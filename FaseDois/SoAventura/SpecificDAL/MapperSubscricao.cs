﻿using SoAventura.DALInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoAventura.Entities;
using System.Transactions;
using System.Data.SqlClient;
using SoAventura.DALAbstraction;
using SoAventura.Utils;

namespace SoAventura.SpecificDAL
{
    public class MapperSubscricao : AbstractMapper<Subscricao, int>, IMapperSubscricao
    {

        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void Create(Subscricao a)
        {
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    SqlCommand cmd = new SqlCommand();

                    StringBuilder command = new StringBuilder("INSERT INTO Subscricao (id_evento, ano, NIF, pago, valor_a_pagar) VALUES (@id_evento, @ano, @NIF, @pago, @valor_a_pagar");
                    if (a.desconto != null)
                    {
                        command.Insert(64, ", desconto_id");
                        command.Append(", @desconto_id");
                        cmd.Parameters.AddWithValue("@desconto_id", a.desconto);
                    }

                    if (a.data_pagamento != null)
                    {
                        command.Insert(64, ", data_pagamento");
                        command.Append(", @data_pagamento");
                        cmd.Parameters.AddWithValue("@data_pagamento", a.data_pagamento);
                    }
                    command.Append(")");
                    cmd.CommandText = command.ToString();

                    cmd.Parameters.AddWithValue("@id_evento", a.id_evento);
                    cmd.Parameters.AddWithValue("@ano", a.ano);
                    cmd.Parameters.AddWithValue("@NIF", a.NIF);
                    cmd.Parameters.AddWithValue("@pago", a.pago);
                    cmd.Parameters.AddWithValue("@valor_a_pagar", a.valor_a_pagar);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        cmd.ExecuteNonQuery();
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
        }

        public override void Delete(Subscricao a)
        {
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required))
                {
                    //Call Delete from MapperFactura
                    if (a.factura != null)
                    {
                        MapperFactura b = new MapperFactura();
                        b.Delete(a.factura);
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "DELETE FROM Subscricao WHERE id = @id_subscricao";
                    cmd.Parameters.AddWithValue("@id_subscricao", a.id);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        cmd.ExecuteNonQuery();
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
        }

        public override Subscricao Read(int id)
        {
            Subscricao a = null;

            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT * FROM Subscricao WHERE id = @id";

                    cmd.Parameters.AddWithValue("@id", id);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        SqlDataReader data = cmd.ExecuteReader();

						if (data.Read()) a = new SubscricaoFactory().create(data);
                    }
                    //Call Read of MapperFactura to get Factura
                    if (a != null) a.factura = new MapperFactura().ReadByIdSub(a.id);

                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }

            return a;
        }

        public ICollection<Subscricao> ReadMultByIdAno(Tuple<int, int> tu)
        {
            ICollection<Subscricao> a = new List<Subscricao>();

            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT * FROM Subscricao WHERE id_evento = @id_evento AND ano = @ano";

                    cmd.Parameters.AddWithValue("@id_evento", tu.Item1);
                    cmd.Parameters.AddWithValue("@ano", tu.Item2);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        SqlDataReader data = cmd.ExecuteReader();

                        while (data.Read())
                        {
                            a.Add(new SubscricaoFactory().create(data));
                        }
                    }
                    MapperFactura subMapper = new MapperFactura();
                    a.ToList().ForEach(sub => sub.factura = subMapper.ReadByIdSub(sub.id));

                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }

            return a;
        }

        public ICollection<int> ReadIdsByNif(int nif)
        {
            ICollection<int> a = new List<int>();

            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT id FROM Subscricao WHERE nif = @nif";

                    cmd.Parameters.AddWithValue("@nif", nif);

                    using (var cn = new SqlConnection(cs))
                    {
                        cmd.Connection = cn;
                        cn.Open();

                        SqlDataReader data = cmd.ExecuteReader();

                        while (data.Read())
                        {
                            a.Add(data.GetSqlInt32(0).Value);
                        }
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }

            return a;
        }

        public override void Update(Subscricao entity)
        {
            throw new NotImplementedException();
        }

    }
}
