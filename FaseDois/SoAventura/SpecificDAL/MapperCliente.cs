﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Transactions;
using System.Data.SqlClient;
using System.Data;
using SoAventura.Entities;
using SoAventura.DALAbstraction;
using SoAventura.DALInterfaces;
using SoAventura.Utils;

namespace SoAventura.SpecificDAL
{
	public class MapperCliente: AbstractMapper<Cliente, int>, IMapperCliente
	{
		private ClienteFactory clientFatory = new ClienteFactory();
		private static readonly log4net.ILog log =
			log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public override Cliente Read(int id)
		{
			Cliente cliente = null;
			try
			{
				using (var ts = new TransactionScope(TransactionScopeOption.Required,
					new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
				{
					SqlCommand cmd = new SqlCommand();
					cmd.CommandText = "select * from Cliente where NIF = @id";
					cmd.Parameters.AddWithValue("@id", id);
					using (var cn = new SqlConnection(this.cs))
					{
						cmd.Connection = cn;
						cn.Open();
						SqlDataReader reader = cmd.ExecuteReader();
						if (!reader.Read()) return null;
						cliente = clientFatory.create(reader);
					}
					cliente.Emails = new MapperEmail().ReadEmailsByNIF(cliente.nif);
					ts.Complete();
				}
			}
			catch (Exception e)
			{
				log.Error(e.Message);
			}
			return cliente;
		}

		public override void Create(Cliente entity)
		{
			throw new NotImplementedException();
		}

		public override void Update(Cliente entity)
		{
			throw new NotImplementedException();
		}

		public override void Delete(Cliente entity)
		{
			throw new NotImplementedException();
		}
	}
}
