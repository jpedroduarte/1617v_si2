﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura.Entities
{
    public class Subscricao
    {
        public int id { get; set; }
        public int id_evento { get; set; }
        public int ano { get; set; }
        public int NIF { get; set; }
        public Nullable<int> desconto { get; set; }
        public Nullable<DateTime> data_pagamento { get; set; }
        public bool pago { get; set; }
        public decimal valor_a_pagar { get; set; }
        public Factura factura { get; set; }

	}
}