﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura.Entities
{
	public class Tipo_Desporto
	{
		public Tipo_Desporto()
		{
			this.Eventos = new HashSet<Evento>();
		}

		public int id { get; set; }
		public string desporto { get; set; }
		public string atributo { get; set; }

		public virtual ICollection<Evento> Eventos { get; set; }
	}
}
