﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura.Entities
{
	public class Cliente
	{
		public int nif { get; set; }
		public int cc { get; set; }
		public string nome { get; set; }
		public string email { get; set; }
		public Nullable<DateTime> data_nascimento { get; set; }

		public virtual ICollection<Email> Emails { get; set; }
	}
}
