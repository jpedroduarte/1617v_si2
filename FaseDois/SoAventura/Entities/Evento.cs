﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura.Entities
{
	public class Evento
	{
		public Evento()
		{
			this.Subscricoes = new HashSet<Subscricao>();
		}

		public int id { get; set; }
		public int ano { get; set; }
		public string descricao { get; set; }
		public int preco { get; set; }
		public DateTime data_realizacao { get; set; }
		public int nParticipantesMin { get; set; }
		public int nParticipantesMax { get; set; }
		public string estado { get; set; }
		public int idade_min { get; set; }
		public int idade_max { get; set; }
		public DateTime data_inscricao_inicial { get; set; }
		public DateTime data_inscricao_final { get; set; }
		public DateTime data_limite_pagamento { get; set; }
		public int id_tipo_desporto { get; set; }
		public int valor_tipo { get; set; }

		public virtual ICollection<Subscricao> Subscricoes { get; set; }
	}
}
