﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.Procedures
{
    public class SubPayment
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool Pay(int idSubscricao)
        {
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "sp_pagamento_subscricao";
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter IdEvento = cmd.Parameters.AddWithValue("@id_subscricao", idSubscricao);
                    IdEvento.Direction = ParameterDirection.Input;

                    using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SoAventura"].ConnectionString))
                    {
                        cmd.Connection = cn;
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        cn.Close();
                    }
                    ts.Complete();
                    return true;
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                return false;
            }
        }
    }
}
