﻿using SoAventura.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.Procedures
{
    public class ListCanceledEvents
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Dictionary<string, int> FromInterval(DateTime initial, DateTime final)
        {
            Dictionary<string, int> eventCount = new Dictionary<string, int>();
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                   
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "sp_list_canceled_events_by_type";
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Start = cmd.Parameters.AddWithValue("@dateInit", initial);
                    Start.Direction = ParameterDirection.Input;

                    SqlParameter End = cmd.Parameters.AddWithValue("@dateFin", final);
                    End.Direction = ParameterDirection.Input;

                    using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SoAventura"].ConnectionString))
                    {
                        
                        cmd.Connection = cn;
                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            int i = 1;
                            String desporto = reader.GetSqlString(i--).ToString();
                            if (!eventCount.ContainsKey(desporto)) { 
                                eventCount.Add(desporto, reader.GetSqlInt32(i--).Value);
                            }
                        }   
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            return eventCount;
        }

    }
}
