﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.Procedures
{
    public class SendEmails
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void EventInDays(int days)
        {
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "sp_warn_client_event_in_x_days";
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Days = cmd.Parameters.AddWithValue("@days", days);
                    Days.Direction = ParameterDirection.Input;

                    using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SoAventura"].ConnectionString))
                    {
                        cmd.Connection = cn;
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        cn.Close();
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
        }
    }
}
