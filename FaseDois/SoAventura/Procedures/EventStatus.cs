﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.Procedures
{
    public class EventStatus
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void update()
        {
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "sp_update_Evento_status";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SoAventura"].ConnectionString))
                    {
                        cmd.Connection = cn;
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        cn.Close();
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
        }
    }
}
