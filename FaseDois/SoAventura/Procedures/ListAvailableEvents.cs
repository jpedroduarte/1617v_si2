﻿using SoAventura.Entities;
using SoAventura.SpecificDAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.Procedures
{
    class ListAvailableEvents
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static ICollection<Evento> FromInterval(DateTime initial, DateTime final)
        {
            ICollection<Evento> eventList = new List<Evento>();
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "sp_available_event_list_in_x_days";
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Start = cmd.Parameters.AddWithValue("@dateInit", initial);
                    Start.Direction = ParameterDirection.Input;

                    SqlParameter End = cmd.Parameters.AddWithValue("@dateFin", final);
                    End.Direction = ParameterDirection.Input;

                    using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SoAventura"].ConnectionString))
                    {

                        cmd.Connection = cn;
                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            int i = 0;
                            int id = reader.GetInt32(i++);
                            int ano = reader.GetInt32(i++);
                            MapperEvento mapperE = new MapperEvento();
                            Evento evento = mapperE.Read(Tuple.Create(id,ano));
                            eventList.Add(evento);
                        }
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            return eventList;
        }
    }
}
