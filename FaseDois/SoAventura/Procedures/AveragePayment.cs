﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.Procedures
{
    class AveragePayment
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Dictionary<int, decimal> FromInterval(int initial, int final)
        {
            Dictionary<int, decimal> paymentList = new Dictionary<int, decimal>();
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "sp_get_payments_by_year";
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Start = cmd.Parameters.AddWithValue("@yearLow", initial);
                    Start.Direction = ParameterDirection.Input;

                    SqlParameter End = cmd.Parameters.AddWithValue("@yearHigh", final);
                    End.Direction = ParameterDirection.Input;

                    using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SoAventura"].ConnectionString))
                    {

                        cmd.Connection = cn;
                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            int i = 1;
                            int ano = reader.GetSqlInt32(i--).Value;
                            if (!paymentList.ContainsKey(ano))
                            {
                                paymentList.Add(ano, reader.GetSqlDecimal(i--).Value);
                            }
                        }
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            return paymentList;
        }
    }
}
