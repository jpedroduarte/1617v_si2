﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoAventura.Procedures
{
    public class EventSubscriptionSP
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool Subscribe(int id_evento, int ano, int nif, int? desconto_id)
        {
            try
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "sp_subscricao_cliente_existente";
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter IdEvento = cmd.Parameters.AddWithValue("@id_evento", id_evento);
                    IdEvento.Direction = ParameterDirection.Input;
                    SqlParameter Ano = cmd.Parameters.AddWithValue("@ano", ano);
                    Ano.Direction = ParameterDirection.Input;
                    SqlParameter Nif = cmd.Parameters.AddWithValue("@NIF", nif);
                    Nif.Direction = ParameterDirection.Input;
                    SqlParameter DescontoId = cmd.Parameters.AddWithValue("@desconto_id", desconto_id);
                    DescontoId.Direction = ParameterDirection.Input;

                    using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SoAventura"].ConnectionString))
                    {
                        cmd.Connection = cn;
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        cn.Close();
                    }
                    ts.Complete();
                    return true;
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                return false;
            }
        }
    }
}
