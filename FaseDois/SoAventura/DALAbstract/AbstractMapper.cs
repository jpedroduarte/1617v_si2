﻿/*
 * Este código foi desenvolvido para exemplificação e consolidação
 * de coceitos da unidade curricular Arquitectura de Sistemas de Informação do
 * Mestredo em Engenharia Informática e de Computadores do Instituto Superior de Engenharia
 * de Lisboa (Institupo Politécnico de Lisboa)
 * Não é completo, nem é destinado a ser usado como peça de produtos informáticos fora 
 * do âmbito pedagógico.
 * 
 * -------------------------------------
 * Autor: Walter Vieira
 * -------------------------------------
 * Data de criaçao: 2012/11/14
 * -------------------------------------
 * História de alterações:
 * 
 * 2012/11/19 - Walter Vieira. Introduzidos estes comentários.
 * 
 * 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SoAventura.DALAbstraction
{
    public abstract class AbstractMapper<T, Tid>
    {
		private static readonly log4net.ILog log =
			log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private static readonly string connectionStringName = "SoAventura";

		protected string cs
		{
			get
			{
				string aux = null;
				try
				{
					aux = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
				}
				catch (ConfigurationErrorsException e)
				{
					log.Fatal(e.Message);
				}
				return aux;
			}
		}

		//CRUD
        public abstract void Create(T entity);
		public abstract T Read(Tid id);
		public abstract void Update(T entity);
		public abstract void Delete(T entity);
    }
}
