﻿using SoAventura.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura.Utils
{
    class PrintResults
    {
        public static void print <TKey, TElement>(Dictionary<TKey,TElement> dic, String key, String value)
        {
            dic.ToList().ForEach((pair) => Console.WriteLine(key + ": " + pair.Key + ", " + value + ": " + pair.Value));
        }

        public static void print (ICollection<Evento> col)
        {
            col.ToList().ForEach((eve) => Console.WriteLine("ID: " + eve.id + ", Year: " + eve.ano));
        }
    }
}
