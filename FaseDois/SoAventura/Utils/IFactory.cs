﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SoAventura.Utils
{
	public interface IFactory<T>
	{
		T create(SqlDataReader reader);
	}
}
