﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoAventura.Entities;
using System.Data.SqlClient;

namespace SoAventura.Utils
{
	public class ClienteFactory : IFactory<Cliente>
	{
		public Cliente create(SqlDataReader reader)
		{
			Cliente cliente = new Cliente();
			int i = 0;
			cliente.nif = reader.GetSqlInt32(i++).Value;
			cliente.cc = reader.GetSqlInt32(i++).Value;
			cliente.nome = reader.GetSqlString(i++).Value;
			cliente.email = reader.GetSqlString(i++).Value;
			cliente.data_nascimento = reader.GetDateTime(i++);
			return cliente;
		}
	}
}
