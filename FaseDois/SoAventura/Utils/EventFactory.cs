﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoAventura.Entities;
using System.Data.SqlClient;

namespace SoAventura.Utils
{
	public class EventFactory : IFactory<Evento>
	{
		public Evento create(SqlDataReader reader){
			Evento evento = new Evento();
			int i = 0;
			evento.id = reader.GetSqlInt32(i++).Value;
			evento.ano = reader.GetSqlInt32(i++).Value;
			evento.descricao = reader.GetSqlString(i++).Value;
			evento.preco = reader.GetSqlInt32(i++).Value;
			evento.data_realizacao = reader.GetDateTime(i++);
			evento.nParticipantesMin = reader.GetSqlInt32(i++).Value;
			evento.nParticipantesMax = reader.GetSqlInt32(i++).Value;
			evento.estado = reader.GetSqlString(i++).Value;
			evento.idade_min = reader.GetSqlInt32(i++).Value;
			evento.idade_max = reader.GetSqlInt32(i++).Value;
			evento.data_inscricao_inicial = reader.GetSqlDateTime(i++).Value;
			evento.data_inscricao_final = reader.GetSqlDateTime(i++).Value;
			evento.data_limite_pagamento = reader.GetSqlDateTime(i++).Value;
			evento.id_tipo_desporto = reader.GetSqlInt32(i++).Value;
			evento.valor_tipo = reader.GetSqlInt32(i++).Value;
			return evento;
		}
	}
}
