﻿using SoAventura.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura.Utils
{
    class FacturaFactory : IFactory<Factura>
    {
        public Factura create(SqlDataReader data)
        {
            Factura a = new Factura();
            int i = 0;

            a.id = data.GetSqlInt32(i++).Value;
            a.id_subcricao = data.GetSqlInt32(i++).Value;
            a.ano = (int)data.GetSqlInt16(i++).Value;

            return a;
        }
    }
}
