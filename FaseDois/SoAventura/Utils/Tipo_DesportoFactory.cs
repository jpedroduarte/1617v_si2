﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using SoAventura.Entities;

namespace SoAventura.Utils
{
	public class Tipo_DesportoFactory : IFactory<Tipo_Desporto>
	{
		public Tipo_Desporto create(SqlDataReader reader)
		{
			Tipo_Desporto tipo = new Tipo_Desporto();
			int i = 0;
			tipo.id = reader.GetSqlInt32(i++).Value;
			tipo.desporto = reader.GetSqlString(i++).Value;
			tipo.atributo = reader.GetSqlString(i++).Value;
			return tipo;
		}
	}
}
