﻿using SoAventura.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoAventura.Utils
{
    class SubscricaoFactory : IFactory<Subscricao>
    {
        public Subscricao create(SqlDataReader data)
        {
            Subscricao a = new Subscricao();
            int i = 0;

            a.id = data.GetSqlInt32(i++).Value;
            a.id_evento = data.GetSqlInt32(i++).Value;
            a.ano = data.GetSqlInt32(i++).Value;
            a.NIF = data.GetSqlInt32(i++).Value;

            System.Data.SqlTypes.SqlInt32 b = data.GetSqlInt32(i++);
            a.desconto = b.IsNull ? null : new Nullable<int>(b.Value);
            System.Data.SqlTypes.SqlDateTime c = data.GetSqlDateTime(i++);
            a.data_pagamento = c.IsNull ? null : new Nullable<DateTime>(c.Value);

            a.pago = data.GetSqlBoolean(i++).Value;
            a.valor_a_pagar = data.GetSqlDecimal(i++).Value;

            return a;
        }
    }
}
