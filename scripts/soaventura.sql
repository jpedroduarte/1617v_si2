use SoAventura;
go
/*--- c) Inserir, remover e actualizar informacao de um evento ---*/
IF OBJECT_ID('sp_insert_Cliente') IS NOT NULL
DROP PROCEDURE sp_insert_Cliente;
go
create proc sp_insert_Cliente
@NIF int,
@CC int = NULL,
@nome varchar(100) = NULL,
@email varchar(100) = NULL,
@data_nascimento date = NULL,
@morada varchar(100) = NULL
as begin
	BEGIN TRY
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRAN
	if exists(select Cliente.NIF from Cliente where Cliente.NIF = @NIF)
		RAISERROR('This client NIF is already taken', 16, 1)

	if @CC is not null AND exists(select CC from Cliente where CC = @CC)
		RAISERROR('This client CC is already taken', 16, 1)

	insert into Cliente(NIF, CC, nome, email, data_nascimento)
		values(@NIF, @CC, @nome, @email, @data_nascimento);
	
	if @morada is not null
		insert into Morada(cliente, morada) values(@NIF, @morada)
		
	COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK;
		THROW
	END CATCH
end
--test the unique NIF
--test the unique CC
exec sp_insert_Cliente 5, 3
go

IF OBJECT_ID('sp_remove_Cliente') IS NOT NULL
DROP PROCEDURE sp_remove_Cliente;
go
create proc sp_remove_Cliente
@NIF int
as begin
	BEGIN TRY
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRAN
		if not exists(select * from Cliente where NIF = @NIF)
			RAISERROR('The client does not exist.', 16, 1)
		--morada
		delete from Morada where Morada.cliente = @NIF
		--emails
		delete from Email_Log where Email_Log.NIF = @NIF
		--facturas
		delete from Factura where id_subscricao IN (select id from Subscricao where NIF = @NIF)
		--subscric�es
		delete from Subscricao where Subscricao.NIF = @NIF
		--Cliente
		delete from Cliente where NIF=@NIF
	COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK;
		THROW
	END CATCH
end
go

IF OBJECT_ID('sp_update_Cliente') IS NOT NULL
DROP PROCEDURE sp_update_Cliente;
go
create proc sp_update_Cliente
@NIF int,
@CC int = NULL,
@nome varchar(100) = NULL,
@email varchar(100) = NULL,
@data_nascimento date = NULL,
@morada varchar(100) = NULL
as begin
	BEGIN TRY
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRAN
	if not exists(select * from Cliente where NIF = @NIF)
		RAISERROR('The client does not exist', 16, 1)
	update Cliente 
	set CC=@CC, nome= @nome, email=@email, data_nascimento=@data_nascimento
	where NIF=@NIF

	if @morada is not null
		insert into Morada(cliente, morada) values(@NIF, @morada)
	COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK;
		THROW
	END CATCH
end
go

/*- Function to get new ID based on year -*/

IF OBJECT_ID('f_getNewEventoId') IS NOT NULL
	DROP FUNCTION f_getNewEventoId
GO

CREATE FUNCTION dbo.f_getNewEventoId (@ano INT) RETURNS INT
AS
	BEGIN
		DECLARE @id INT = 1
		SELECT TOP 1 @id += id FROM Evento WHERE ano=@ano ORDER BY id DESC
		RETURN @id
	END
GO	

/*--- d) Inserir, remover e actualizar informacao de um evento ---*/

IF OBJECT_ID('sp_inserir_Evento') IS NOT NULL
	DROP PROCEDURE sp_inserir_Evento
GO

CREATE PROC sp_inserir_Evento @descricao VARCHAR(300), @preco INT, @data_realizacao DATE, @nParticipantesMin INT, @nParticipantesMax INT, @estado VARCHAR(50), @idade_min INT, @idade_max INT, @data_inscricao_inicial DATETIME, @data_inscricao_final DATETIME, @data_limite_pagamento DATETIME, @id_tipo_desporto INT, @valor_tipo INT
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRAN
		DECLARE @date DATETIME = GETDATE()
		DECLARE @ano INT = YEAR(@date)
		
		IF @preco>0 AND @data_realizacao>CAST(@date AS DATE) AND @nParticipantesMin<=@nParticipantesMax AND (@estado = 'em subscricao' OR @estado = 'subscrito' OR @estado = 'concluido' OR @estado = 'cancelado') AND @idade_min<=@idade_max AND @data_inscricao_inicial<=@data_inscricao_final AND @data_limite_pagamento>=@data_inscricao_final AND @valor_tipo > 0
			INSERT INTO Evento(id, ano, descricao, preco, data_realizacao, nParticipantesMin, nParticipantesMax, estado, idade_min, idade_max, data_inscricao_inicial, data_inscricao_final, data_limite_pagamento, id_tipo_desporto, valor_tipo) values
				(dbo.f_getNewEventoId(@ano),@ano,@descricao,@preco,@data_realizacao,@nParticipantesMin,@nParticipantesMax,@estado,@idade_min,@idade_max,@data_inscricao_inicial,@data_inscricao_final, @data_limite_pagamento,@id_tipo_desporto, @valor_tipo)
		
		COMMIT
GO

IF OBJECT_ID('sp_remover_Evento') IS NOT NULL
	DROP PROCEDURE sp_remover_Evento
GO

CREATE PROC sp_remover_Evento @id INT, @ano INT
AS
	BEGIN TRY
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRAN
		DELETE FROM Factura WHERE id_subscricao IN (SELECT id FROM Subscricao WHERE id_evento = @id AND ano = @ano)
		DELETE FROM Subscricao WHERE id_evento = @id AND ano = @ano
		DELETE FROM Evento WHERE id = @id AND ano = @ano
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK;
		THROW;
	END CATCH
GO

IF OBJECT_ID('sp_update_Evento') IS NOT NULL
	DROP PROCEDURE sp_update_Evento
GO

CREATE PROC sp_update_Evento @id INT, @ano INT, @descricao VARCHAR(300) = NULL, @preco INT = NULL, @data_realizacao DATE = NULL, @nParticipantesMin INT = NULL, @nParticipantesMax INT = NULL, @estado VARCHAR(50) = NULL, @idade_min INT = NULL, @idade_max INT = NULL, @data_inscricao_inicial DATETIME = NULL, @data_inscricao_final DATETIME = NULL, @data_limite_pagamento DATETIME = NULL, @id_tipo_desporto INT = NULL, @valor_tipo INT = NULL
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRAN	
		DECLARE @descricaoT VARCHAR(300), @precoT INT, @data_realizacaoT DATE, @nParticipantesMinT INT, @nParticipantesMaxT INT, @estadoT VARCHAR(50), @idade_minT INT, @idade_maxT INT, @data_inscricao_inicialT DATETIME, @data_inscricao_finalT DATETIME, @data_limite_pagamentoT DATETIME, @id_tipo_desportoT INT, @valor_tipoT INT
		SELECT @descricaoT=descricao , @precoT=preco, @data_realizacaoT=data_realizacao, @nParticipantesMinT=nParticipantesMin, @nParticipantesMaxT=nParticipantesMax, @estadoT=estado, @idade_minT=idade_min, @idade_maxT=idade_max, @data_inscricao_inicialT=data_inscricao_inicial, @data_inscricao_finalT=data_inscricao_final, @data_limite_pagamentoT=data_limite_pagamento, @id_tipo_desportoT=id_tipo_desporto, @valor_tipoT=@valor_tipo FROM Evento WHERE id = @id AND ano = @ano

		IF @descricao IS NOT NULL SET @descricaoT=@descricao
		IF @preco IS NOT NULL SET @precoT=@preco
		IF @data_realizacao IS NOT NULL SET @data_realizacaoT=@data_realizacao
		IF @nParticipantesMin IS NOT NULL SET @nParticipantesMinT=@nParticipantesMin
		IF @nParticipantesMax IS NOT NULL SET @nParticipantesMaxT=@nParticipantesMax
		IF @estado IS NOT NULL SET @estadoT=@estado
		IF @idade_min IS NOT NULL SET @idade_minT=@idade_min
		IF @idade_max IS NOT NULL SET @idade_maxT=@idade_max
		IF @data_inscricao_inicial IS NOT NULL SET @data_inscricao_inicialT=@data_inscricao_inicial
		IF @data_inscricao_final IS NOT NULL SET @data_inscricao_finalT=@data_inscricao_final
		IF @data_limite_pagamento IS NOT NULL SET @data_limite_pagamentoT=@data_limite_pagamento
		IF @id_tipo_desporto IS NOT NULL SET @id_tipo_desportoT=@id_tipo_desporto
		IF @valor_tipo IS NOT NULL SET @valor_tipoT=@valor_tipo

		IF @precoT>0 AND @data_realizacaoT>CAST(GETDATE() AS DATE) AND @nParticipantesMinT<=@nParticipantesMaxT AND (@estadoT = 'em subscricao' OR @estadoT = 'subscrito' OR @estadoT = 'concluido' OR @estadoT = 'cancelado') AND @idade_minT<=@idade_maxT AND @data_inscricao_inicialT<=@data_inscricao_finalT AND @data_limite_pagamentoT>=@data_inscricao_finalT AND @valor_tipoT>0
			UPDATE Evento SET descricao=@descricaoT, preco=@precoT, data_realizacao=@data_realizacaoT, nParticipantesMin=@nParticipantesMinT, nParticipantesMax=@nParticipantesMaxT, estado=@estadoT, idade_min=@idade_minT, idade_max=@idade_maxT, data_inscricao_inicial=@data_inscricao_inicialT, data_inscricao_final=@data_inscricao_finalT, data_limite_pagamento=@data_limite_pagamentoT, id_tipo_desporto=@id_tipo_desportoT, valor_tipo=@valor_tipoT WHERE id = @id AND ano = @ano
		/*ELSE RAISERROR*/

		COMMIT
GO

/*
f)Proceder ao pagamento de uma subscri��o
*/
IF OBJECT_ID('sp_pagamento_subscricao') IS NOT NULL
DROP PROCEDURE sp_pagamento_subscricao;
go
create proc sp_pagamento_subscricao
@id_subscricao int
as begin
	BEGIN TRY
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRAN
		if not exists(select * from Subscricao where id= @id_subscricao)
			RAISERROR('The subscription does not exist', 16, 1)
		
		--check if the subscription is already paid
		if exists(select * from Subscricao where id= @id_subscricao and pago = 1)
			RAISERROR('The subscription is already paid', 16, 1)
		declare @currdate datetime = GETDATE()
		--update the payment state on Subscricao to paid.
		update Subscricao
			set data_pagamento = @currdate, pago=1
			where id=@id_subscricao

		declare 
			@id_factura int = 1, 
			@ano_factura int = YEAR(@currdate)

		--get next id from Factura
		select @id_factura = MAX(id) + @id_factura  from Subscricao where ano = @ano_factura
		
		--pode haver aqui inconsistencia de dados (solucao: repeatable read)

		--insert new factura
		insert into Factura(id, id_subscricao, ano) values
			(@id_factura, @id_subscricao, @ano_factura)

		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK;
		THROW;
	END CATCH
end
go

/*
 e) Realizar a subscricao de um evento por parte de um cliente existente
*/

/*Test
delete Subscricao
select * from Subscricao
insert into Subscricao(id_evento, ano, NIF, data_pagamento, pago, valor_a_pagar, desconto_id) values
	(1, 2016, 0, '2016-05-03T23:59:59', 1, 200, 2)
*/

IF OBJECT_ID('f_algoritmo_desconto') IS NOT NULL
DROP FUNCTION f_algoritmo_desconto

go

create function dbo.f_algoritmo_desconto
(
@valor_bruto int,
@valor_desconto int
)
returns int
as
begin
	return @valor_bruto - @valor_desconto
end

go

IF OBJECT_ID('f_valor_total_pagamento') IS NOT NULL
DROP FUNCTION f_valor_total_pagamento;

go

create function f_valor_total_pagamento
(
@id_subscricao int
)
returns int
as
begin
	declare @valor_bruto int
	declare @valor_desconto int
	declare @id_evento int
	declare @ano int
	--obter id_evento e ano
	select @id_evento=id_evento, @ano=ano from Subscricao where Subscricao.id= @id_subscricao
	--obter preco
	select @valor_bruto=preco from Evento where Evento.id= @id_evento AND Evento.ano = @ano
	--obter Desconto
	select @valor_desconto = Desconto.value
	from Subscricao INNER JOIN Desconto ON Subscricao.desconto_id = Desconto.id
	where Subscricao.id= @id_subscricao
	if @valor_desconto is null
		return @valor_bruto
	else
		begin
			declare @valor_final int
			--aplicar algoritmo de desconto 
			set @valor_final = dbo.f_algoritmo_desconto(@valor_bruto, @valor_desconto)
		end
	return @valor_final
end

go
/* Test
declare @bla int
set @bla= dbo.f_valor_total_pagamento(1)
print @bla
select * from Evento where Evento.id=1
select * from Subscricao
select * from Desconto
update Subscricao
set desconto_id = 2
where id = 1
delete Desconto
DBCC CHECKIDENT ('Desconto', RESEED, 0);
*/
go

/*--- e) Realizar a subscri��o de um evento por parte de um cliente existente ---*/
IF OBJECT_ID('sp_subscricao_cliente_existente') IS NOT NULL
DROP PROCEDURE sp_subscricao_cliente_existente;
go
create proc sp_subscricao_cliente_existente
@id_evento int,
@ano int,
@NIF int,
@desconto_id int = NULL
as
	BEGIN TRY
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRAN
	/* Verify if it is possible to execute with the inut given accordingly */
	declare @data_inscricao_final datetime, @data_inscricao_inicial datetime, @data_currente datetime = GETDATE()

	--get dates from the event to assert if it is possible to execute the subscription
	select @data_inscricao_final = Evento.data_inscricao_final,
		@data_inscricao_inicial = Evento.data_inscricao_inicial
		from Evento where id = @id_evento AND ano = @ano

	if @data_inscricao_final <= @data_currente OR @data_inscricao_inicial >= @data_currente
		RAISERROR('could not subscribe the event because the current date is invalid for this event', 16, 1)

	if NOT exists(select NIF from Cliente where NIF = @NIF)
		RAISERROR('could not subscribe the event because the NIF is invalid', 16, 1)

	/* Logic Operations */
	insert into Subscricao(id_evento, ano, NIF, desconto_id) values
		(@id_evento, @ano, @NIF, @desconto_id);

	update Subscricao set valor_a_pagar = dbo.f_valor_total_pagamento(@@IDENTITY) where id = @@IDENTITY
	COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK;
		THROW;
	END CATCH
go

/*--- g) Transitar todos os eventos de um estado, em funcao da data corrente ---*/

IF OBJECT_ID('sp_update_Evento_status') IS NOT NULL
	DROP PROCEDURE sp_update_Evento_status
GO

CREATE PROC sp_update_Evento_status
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRAN
		SET NOCOUNT ON
		DECLARE @date DATETIME = GETDATE()
		UPDATE Evento SET Evento.estado = 'subscrito' FROM (SELECT Evento.estado, COUNT(Subscricao.NIF) AS c, Evento.id, Evento.ano FROM Evento INNER JOIN Subscricao ON Subscricao.id_evento=Evento.id AND Subscricao.ano=Evento.ano GROUP BY  Evento.id, Evento.ano, Evento.estado, Evento.nParticipantesMin HAVING COUNT(Subscricao.NIF) >= Evento.nParticipantesMin) AS grouped WHERE grouped.estado = 'em subscricao' AND Evento.id = grouped.id AND Evento.ano = grouped.ano
		UPDATE Evento SET estado = 'concluido' WHERE data_realizacao < @date AND estado = 'subscrito'
		UPDATE Evento SET estado = 'cancelado' WHERE data_inscricao_final < @date AND estado = 'em subscricao'
	COMMIT
GO

/*- Send Mails as asked in project -*/

IF OBJECT_ID('SendMail') IS NOT NULL
	DROP PROCEDURE SendMail
Go

CREATE PROC SendMail @nif INT, @text VARCHAR(400)
AS
	BEGIN
		INSERT INTO Email_Log VALUES (@nif,@text)
	END
GO

/*--- h) Enviar email da realizacao do evento em X dias ---*/

IF OBJECT_ID('sp_warn_client_event_in_x_days') IS NOT NULL
	DROP PROCEDURE sp_warn_client_event_in_x_days
GO

CREATE PROC sp_warn_client_event_in_x_days @days INT
AS
	BEGIN TRAN
		SET NOCOUNT ON
		
		DECLARE @date DATETIME = DATEADD(day,@days,GETDATE())

		DECLARE @cursor as CURSOR

		SET @cursor = CURSOR FOR
			SELECT Cliente.NIF, DATEDIFF(day,GETDATE(),Evento.data_realizacao) AS days FROM Cliente
			INNER JOIN Subscricao ON Cliente.NIF = Subscricao.NIF
			INNER JOIN Evento ON Evento.id = Subscricao.id_evento AND Evento.ano = Subscricao.ano
			WHERE Evento.data_realizacao < @date AND Evento.data_realizacao > GETDATE()

		OPEN @cursor

		DECLARE @nif INT, @day INT, @text VARCHAR (100)

		SET @text = 'Your event starts in '
		FETCH NEXT FROM @cursor INTO @nif, @day;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @text += CAST(@day AS VARCHAR(10))
			SET @text += ' days. Dont forget and dont miss it!'
			EXEC SendMail @nif, @text

			SET @text = 'Your event starts in '
			FETCH NEXT FROM @cursor INTO @nif, @day;
		END

		CLOSE @cursor
		DEALLOCATE @cursor

	COMMIT
GO

/*--- i) Listar a contagem dos eventos cancelados, agrupados por tipo ---*/

IF OBJECT_ID('sp_list_canceled_events_by_type') IS NOT NULL
	DROP PROCEDURE sp_list_canceled_events_by_type
GO

CREATE PROC sp_list_canceled_events_by_type @dateInit DATE, @dateFin DATE
AS
	BEGIN
		SELECT COUNT(Evento.estado) AS count, Tipo_Desporto.desporto FROM Evento
		INNER JOIN Tipo_Desporto ON Tipo_Desporto.id=Evento.id_tipo_desporto
		WHERE Evento.estado = 'cancelado' AND @dateInit <= Evento.data_realizacao AND Evento.data_realizacao <= @dateFin
		GROUP BY Tipo_Desporto.desporto
	END
GO

/*--- j) Listar os eventos com lugares disponiveis num intervalo ---*/

IF OBJECT_ID('sp_available_event_list_in_x_days') IS NOT NULL
	DROP PROCEDURE sp_available_event_list_in_x_days
GO

CREATE PROC sp_available_event_list_in_x_days
@dateInit DATETIME,
@dateFin DATETIME
AS
	Begin
		SELECT Evento.id, Evento.ano, Evento.descricao, Evento.preco FROM Evento
		WHERE Evento.estado = 'em subscricao' AND (@dateInit <= Evento.data_realizacao AND @dateFin >= Evento.data_realizacao)
		AND Evento.nParticipantesMax > (SELECT COUNT (*) FROM Subscricao WHERE Subscricao.id_evento = Evento.id AND Subscricao.ano = Evento.ano) 
	END
GO

/*--- k) Obter m�dia os pagamentos por ano entre certos anos ---*/

IF OBJECT_ID('sp_get_payments_by_year') IS NOT NULL
	DROP PROCEDURE sp_get_payments_by_year
GO

CREATE PROC sp_get_payments_by_year @yearLow INT, @yearHigh INT
AS
	BEGIN
		SELECT AVG(Subscricao.valor_a_pagar) AS Average, YEAR(Subscricao.data_pagamento) as Year FROM Subscricao WHERE YEAR(Subscricao.data_pagamento) >= @yearLow AND YEAR(Subscricao.data_pagamento) <= @yearHigh GROUP BY YEAR(Subscricao.data_pagamento)
	END
GO

/*--- k2) Obter pagamentos dum dado ano entre certos valores ---*/

/*IF OBJECT_ID('sp_get_payments_by_year') IS NOT NULL
	DROP PROCEDURE sp_get_payments_by_year
GO

CREATE PROC sp_get_payments_by_year @year INT, @valueLow INT, @valueHigh INT
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRAN
		SELECT Factura.ano, Subscricao.data_pagamento, Subscricao.valor_a_pagar, Cliente.NIF, Evento.descricao FROM Factura
		INNER JOIN Subscricao ON Subscricao.id = Factura.id_subscricao
		INNER JOIN Cliente ON Cliente.NIF = Subscricao.NIF
		INNER JOIN Evento ON Evento.id = Subscricao.id_evento AND Evento.ano = Subscricao.ano
		WHERE Factura.ano = @year AND @valueLow <= Subscricao.valor_a_pagar AND Subscricao.valor_a_pagar <= @valueHigh
	COMMIT
GO*/