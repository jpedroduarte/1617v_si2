use SoAventura;
GO

IF OBJECT_ID('Morada') IS NOT NULL
DROP TABLE Morada
IF OBJECT_ID('Email_Log') IS NOT NULL
DROP TABLE Email_Log
IF OBJECT_ID('Factura') IS NOT NULL
DROP TABLE Factura
IF OBJECT_ID('Subscricao') IS NOT NULL
DROP TABLE Subscricao
IF OBJECT_ID('Desconto') IS NOT NULL
DROP TABLE Desconto
IF OBJECT_ID('Cliente') IS NOT NULL
DROP TABLE Cliente
IF OBJECT_ID('Evento') IS NOT NULL
DROP TABLE Evento
IF OBJECT_ID('Tipo_Desporto') IS NOT NULL
DROP TABLE Tipo_Desporto
GO

/**************************************************************************** CREATE TABLE ***********************************************************************/

create table Tipo_Desporto(
id int identity(1,1) primary key,
desporto varchar(50) NOT NULL,
atributo varchar(50) NOT NULL
);

create table Evento
(
id	int,						
ano int,
descricao varchar(300) NOT NULL,
preco int NOT NULL,
data_realizacao date NOT NULL,
nParticipantesMin int NOT NULL,
nParticipantesMax int NOT NULL,
estado varchar(50) NOT NULL, --Acho que se devia criar uma tabela para os estados
idade_min int NOT NULL, --Idem
idade_max int NOT NULL,
data_inscricao_inicial datetime NOT NULL,
data_inscricao_final datetime NOT NULL,
data_limite_pagamento datetime NOT NULL,
id_tipo_desporto int NOT NULL,
valor_tipo int NOT NULL,
foreign key(id_tipo_desporto) references Tipo_Desporto(id),
primary key(id, ano)
);

create table Cliente(
NIF int primary key,
CC int UNIQUE NULL,
nome varchar(100) NULL,
email varchar(100) NULL,
data_nascimento date NULL,
); 

create table Morada(
id int identity(1,1) primary key,
cliente int NOT NULL,
morada varchar(200),
foreign key(cliente) references Cliente(NIF)
);

create table Desconto(
id int identity(1,1) NOT NULL,
value int NOT NULL,
descricao varchar(50),
primary key(id)
);

create table Subscricao(
id int identity(1,1) PRIMARY KEY,
id_evento int NOT NULL,
ano int NOT NULL,
NIF int NOT NULL,
desconto_id int default NULL,
data_pagamento datetime NULL,
pago bit NOT NULL default 0,
valor_a_pagar decimal(19,4),
foreign key(NIF) references Cliente(NIF),
foreign key(id_evento, ano) references Evento(id, ano),
foreign key(desconto_id) references Desconto(id),
CONSTRAINT uq_Subscricao UNIQUE (id_evento, ano, NIF)
);

create table Factura(
id int NOT NULL,
id_subscricao int NOT NULL,
ano smallint NOT NULL,
foreign key(id_subscricao) references Subscricao(id),
primary key(id,ano)
);

create table Email_Log(
id int identity(1,1) primary key,
NIF int foreign key references Cliente(NIF),
mensagem varchar(400) NOT NULL
);