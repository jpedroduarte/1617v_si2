--c)
	select * FROM Cliente
	exec sp_insert_Cliente 4, 000000004, 'pessoa', 'pessoa@email.com', '1980-09-13'
	exec sp_insert_Cliente 2
	select * FROM Cliente

	select * FROM CLiente
	exec sp_remove_Cliente 2
	select * from Cliente
	select * from Morada where Morada.cliente = 0
	select * from Factura where id_subscricao IN (select id from Subscricao where NIF = 0)
	select * from Subscricao where NIF = 0
	exec sp_remove_Cliente 0
	select * FROM CLiente

	select * from Cliente
	exec sp_update_Cliente 000000006, 7,'pessoa', 'pessoa@email.com', '1980-09-13'
	exec sp_update_Cliente 000000002, 4,'pessoa', 'pessoa@email.com', '1980-09-13'
	exec sp_update_Cliente 0
	select * from Cliente

--d)
	select * from Evento
	exec sp_inserir_Evento 'Escalada ao monte', 200, '2016-05-23', 2, 10, 'concluido', 16, 70,'2016-01-10T23:59:59', '2016-04-30T23:59:59', '2016-05-10T23:59:59', 4
	exec sp_inserir_Evento 'Escalada ao monte', 200, '2017-05-23', 2, 10, 'concluido', 16, 70,'2017-01-10T23:59:59', '2017-04-30T23:59:59', '2017-05-10T23:59:59', 4
	select * from Evento

	Select * from Evento
	Select * from Factura
	Select * from Subscricao
	exec sp_remover_Evento 2, 2016
	Select * from Evento
	Select * from Factura
	Select * from Subscricao

	Select * from Evento
	exec sp_update_Evento 1, 2017, 'Alterado', 1, '2016-06-10', 3, 30, null , 12, 80, '2016-03-10T23:59:59', '2016-05-24T23:59:59', '2016-06-01T23:59:59', 3
	Select * from Evento

--e)
	select * from Cliente
	select * from Evento
	select * from Subscricao
	exec sp_subscricao_cliente_existente 2, 2017,0 ,null
	exec sp_subscricao_cliente_existente 2, 2017,1 ,null
	select * from Subscricao

--f)
	select * from Subscricao
	exec sp_pagamento_subscricao 6
	select * from Subscricao
	select * from Factura
	exec sp_pagamento_subscricao 8

--g)
	Select * from Evento
	exec sp_update_Evento_status
	select * from Evento

--h)
	select * from Evento
	select * from Email_log
	exec sp_warn_client_event_in_x_days 365
	select * from Subscricao
	select * from Factura
	select * from Email_log
	--N�o envia para eventos que acontecem no pr�prio dia em que � enviado.

--i)
	select *  from Evento
	exec sp_list_canceled_events_by_type '2015-01-01', '2017-03-9'
	exec sp_list_canceled_events_by_type '2017-03-10', '2017-03-10'
	exec sp_list_canceled_events_by_type '2017-03-10', '2017-03-11'
	exec sp_list_canceled_events_by_type '2017-03-11', '2017-03-12'

--j)
	select *  from Evento
	select * from Subscricao
	exec sp_available_event_list_in_x_days '2015-01-01', '2017-03-9'
	exec sp_available_event_list_in_x_days '2017-03-10', '2018-04-22'
	exec sp_available_event_list_in_x_days '2017-03-10', '2019-01-11'

--k)
	select * from Factura
	select * from Subscricao
	exec sp_get_payments_by_year 2016, 2017