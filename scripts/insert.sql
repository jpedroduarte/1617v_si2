use SoAventura;

insert into Cliente(NIF, CC, nome, email, data_nascimento) values
	(0, 000000000, 'Joao Duarte','jduarte@gmail.com', '1993-10-09'), 
	(1, 000000001, 'Filipe Quaresma','bounty@gmail.com', '1992-04-19'),
	(2, 000000002, 'Ligi Sequeira', 'ligi@gmail.com','1980-03-16'),
	(3, 000000003, 'Mota', 'mota@gmail.com', '1993-02-13')
	;

insert into Morada(cliente, morada) values
	(0, '123 6th St.Melbourne, FL 32904'),
	(1, '4 Goldfield Rd.Honolulu, HI 96815'),
	(2, '71 Pilgrim Avenue Chevy Chase, MD 20815'),
	(3, '44 Shirley Ave.West Chicago, IL 60185')
	;

insert into Email_Log(NIF, mensagem) values
	(0, 'Factura qualquer coisa muito cara'),
	(0, 'Bla')
	;

insert into Tipo_Desporto(desporto, atributo) values
	('Canoagem','dificuldade'),
	('Escalada','dificuldade'),
	('Trail','distancia'),
	('Ciclismo','distancia')
	;

insert into Evento(id, ano, descricao, preco, data_realizacao, nParticipantesMin,
 nParticipantesMax, estado, idade_min, idade_max, data_inscricao_inicial,
  data_inscricao_final, data_limite_pagamento, id_tipo_desporto, valor_tipo) values

  (1, 2016, 'Escalada ao monte Everest', 200, '2016-05-23', 2, 10, 'concluido', 16, 70,
  '2016-01-10T23:59:59', '2016-04-30T23:59:59', '2016-05-10T23:59:59', 2, 1),

  (2, 2016, 'Rapidos', 70, '2016-06-10', 3, 30, 'concluido', 12, 80,
  '2016-03-10T23:59:59', '2016-05-24T23:59:59', '2016-06-01T23:59:59', 1, 6),
  
  (3, 2016, 'Sobre rodas', 15, '2018-05-15', 100, 2000, 'em subscricao', 5, 100,
  '2017-06-10T23:59:59', '2017-06-20T23:59:59', '2017-07-01T23:59:59', 4, 10),
  
  (1, 2017, 'Trail', 2000, '2017-03-10', 200, 400, 'cancelado', 16, 80,
  '2017-01-10T23:59:59', '2017-02-28T23:59:59', '2017-03-05T23:59:59', 3, 3),
  
  (2, 2017, 'Escalada horizontal.', 100, '2018-04-20', 1, 3, 'em subscricao', 16, 80,
  '2017-02-07T23:59:59', '2017-07-30T23:59:59', '2017-08-10T23:59:59', 2, 5),

  (3, 2017, 'Going Up', 100, '2017-05-07', 1, 2, 'subscrito', 16, 80,
  '2017-02-07T23:59:59', '2017-04-30T23:59:59', '2017-05-05T23:59:59', 2, 15)
  ;
							
insert into Subscricao(id_evento, ano, NIF, data_pagamento, pago, valor_a_pagar) values
	(1, 2016, 2, '2016-05-09T23:59:59', 1, 200),
	(1, 2016, 0, '2016-05-03T23:59:59', 1, 200),
	(2, 2016, 3, '2016-05-26T23:59:59', 1, 70),
	(2, 2016, 2, '2016-05-31T23:59:59', 1, 70),
	(2, 2016, 0, '2016-05-26T23:59:59', 1, 70),
	(2, 2016, 1, NULL, 0, 70),
	(3, 2016, 1, NULL, 0, 100),
	(2, 2017, 0, '2017-03-31T23:59:59', 1, 100),
	(3, 2017, 3, '2017-03-31T23:59:59', 1, 100)
	;

insert into Desconto(value, descricao) values
	(1, 'Bronze Membership'),
	(10, 'Silver Membership'),
	(30, 'Gold Membership')
	;
insert into Factura(id, id_subscricao, ano) values
	(1, 1, 2016),
	(2, 2, 2016),
	(3, 3, 2016),
	(4, 4, 2016),
	(5, 5, 2016),
	(6, 8, 2017)
	;

	